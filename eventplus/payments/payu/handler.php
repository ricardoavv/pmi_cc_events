<?php
/**
PayU handler
*/
class EventPlus_Payments_PayU_Handler {

    function handleResponse(){

        if (isset($_REQUEST['eventplus_pm']) && isset($_REQUEST['tx'])) {



            if (strtolower($_REQUEST['eventplus_pm']) == 'payu') {
                if (isset($_REQUEST['eventplus_token']) == false) {
                    wp_die("Invalid paypal request.");
                }

                $validActions = array('re7urn' => 'do_return', 'canc3l' => 'do_cancel');

                if (isset($_REQUEST['eventplus_pm']) == false) {
                    wp_die("Oops! Invalid paypal action.");
                }

                $_REQUEST['eventplus_pm_action'] = trim(strtolower($_REQUEST['eventplus_pm_action']));

                if (isset($validActions[$_REQUEST['eventplus_pm_action']]) == false) {
                    wp_die("Invalid paypal action.");
                }

                $method = $validActions[$_REQUEST['eventplus_pm_action']];

                $this->$method();
            }
        }

    }

    /*Handle return*/
    function do_return() {
        global $wpdb;
        exit;
    }

    /*Handle cancel*/
    function do_cancel() {

        global $wpdb;


        $eventplus_token = $_REQUEST['eventplus_token'];

        $company_options = EventPlus_Models_Settings::getSettings();

        $sql = "SELECT * FROM " . get_option('evr_attendee') . " WHERE token = '" . esc_sql($eventplus_token) . "' LIMIT 1";
        $attendeeRow = $wpdb->get_row($sql, ARRAY_A);

        $sql = "SELECT * FROM " . get_option('evr_event') . " WHERE id=" . (int) $attendeeRow['event_id'] . " LIMIT 1";
        $eventRow = $wpdb->get_row($sql, ARRAY_A);
        if ($eventRow['id'] <= 0) {
            wp_die(__("Invalid request", 'evrplus_language'));
        }

        $event_id = $eventRow['id'];


        $returnUrl = evrplus_permalink($company_options['evrplus_page_id']) . "?action=confirmation&eventplus_token=" . $eventplus_token . "&event_id=" . $event_id;

        echo'<script>window.location.href="' . $returnUrl . '";</script>';
        exit;
    }
}
