<?php /* * *******************Pay Configuration******************** */ ?>

<p>
    <label for="use_payu_sandbox">
        <?php _e('Sanbox Mode?', 'evrplus_language'); ?>
        <font size="-6">
        <?php _e('(used for testing/debug)', 'evrplus_language'); ?>
        </font>
    </label>
    <br />
<div class="styled">
    <select name = 'use_payu_sandbox' class="regular-select">

        <option value="Y" <?php if ($company_options['use_payu_sandbox'] == 'Y') echo ' selected'; ?>>
            <?php _e('Yes', 'evrplus_language'); ?>
        </option>
        <option value="N" <?php if ($company_options['use_payu_sandbox'] == 'N') echo ' selected'; ?>>
            <?php _e('No', 'evrplus_language'); ?>
        </option>
    </select>
</div>
</p>

<p>
    <label for="payu_merchantId">
        <?php _e('PayU MerchantId', 'evrplus_language'); ?>
    </label>
    <br />
    <input name="payu_merchantId" id="payu_merchantId" value="<?php echo $company_options['payu_merchantId']; ?>" class="regular-text" type="text"/>
</p>

<p>
    <label for="payu_APILogin">
        <?php _e('PayU API Login', 'evrplus_language'); ?>
    </label>
    <br />
    <input id="payu_APILogin" type="text" name="payu_APILogin" value="<?php echo $company_options['payu_APILogin']; ?>" class="regular-text" />
</p>

<p>
    <label for="payu_APIKey">
        <?php _e('PayU API Key', 'evrplus_language'); ?>
    </label>
    <br />
    <input id="payu_APIKey" type="text" name="payu_APIKey" value="<?php echo $company_options['payu_APIKey']; ?>" class="regular-text" />
</p>

<p>
    <label for="payu_AccountId">
        <?php _e('PayU Account Id', 'evrplus_language'); ?>
    </label>
    <br />
    <input id="payu_AccountId" type="text" name="payu_AccountId" value="<?php echo $company_options['payu_AccountId']; ?>" class="regular-text" />
</p>